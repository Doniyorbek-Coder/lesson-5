// Task 1
function countVowel(str) { 
    const count = str.match(/[aeiouAEIOU]/gi).length;
    return count;
}
const string = 'The quick brown fox';
const result = countVowel(string);
console.log(`Vowels count ${result}`);

// Task 2
function countDays() {
    let today = new Date();
    let newYear = today.getFullYear()+1;
    const newYearDate = new Date(`Jan 1 ${newYear} 00:00:00`);
    let until = newYearDate - today;
    let oneDay = 24*60*60*1000;
    let days = Math.floor(until / oneDay);
    return days;
  }
  console.log(`${countDays()} days left until the New Year`)

  // Task 3
  let arr = [];
  let str = ['w', '%', '#', '$', '3', '&', '0']
  function findSymbol(str) {
    const symbolList = /[-!$%^&*()_+|~=`{}\[\]:";'<>?,.\/@#]/gi;
    for (let i = 0; i < str.length; i++) {
      if (str[i].match(symbolList)) {
        arr.push(str[i]);
      }
    }
    console.log(arr);
  }
  findSymbol(str);

  // Task 4 
  let arr1 = [1, 2, 3, 4 ,6];
  let arr2 = [2, 3, 4, 5, 8];
  function united(arr1, arr2){
    var unit = arr1.filter(e => arr2.includes(e))
    console.log(unit);
}
united(arr1, arr2)


// Task 5 
// setTimeout(function(){
//     alert('Assalomu alaykum')
// }, 4000);

// Task 6
function sumNaturals(num) {
  let sumNat = 0;
  for (let i = 1; i <= num; i++) {
    sumNat += i;
  }
  console.log(sumNat);
}
sumNaturals(24);

// Task 7
function countChar(s, l) {
  const response = new RegExp(l, "g");
  const numbers = s.match(response).length;
  return numbers;
}
console.log(countChar("This is count string", "i"));

  
 
  